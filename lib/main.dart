import 'package:flutter/material.dart';
import 'package:loginapp/homep.dart';
import 'package:loginapp/register.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'login App',
      home: loginpage(),
    );
  }
}

// ignore: camel_case_types
class loginpage extends StatefulWidget {
  const loginpage({Key? key}) : super(key: key);

  @override
  State<loginpage> createState() => _loginpageState();
}

// ignore: camel_case_types
class _loginpageState extends State<loginpage> {
  @override
  Widget build(BuildContext context) {
    // ignore: prefer_typing_uninitialized_variables

    return Scaffold(
      appBar: AppBar(title: const Text("login")),
      body: SafeArea(
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          children: <Widget>[
            Column(
              children: <Widget>[
                const SizedBox(
                  height: 120, //120
                ),
                Image.asset('assets/login1.png'),
                const SizedBox(
                  height: 5.0,
                ),
                const Text(
                  'Login Account',
                  style: TextStyle(fontSize: 25, color: Colors.black),
                )
              ],
            ),
            const SizedBox(
              height: 60.0, //60
            ),
            const TextField(
              decoration: InputDecoration(
                labelText: "Email",
                labelStyle: TextStyle(fontSize: 20),
                filled: true,
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                labelText: "Password",
                labelStyle: TextStyle(fontSize: 20),
                filled: true,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Column(
              children: <Widget>[
                ButtonTheme(
                  disabledColor: Colors.blueAccent,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (BuildContext context) {
                          return const HomePage();
                        }),
                      );
                    },
                    child: const Text('LOGIN'),
                    style: ElevatedButton.styleFrom(
                        fixedSize: const Size(100, 20),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text('New user? Click register below'),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: <Widget>[
                    ButtonTheme(
                      disabledColor: Colors.blueAccent,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (BuildContext context) {
                              return const Registerpage();
                            }),
                          );
                        },
                        child: const Text('REGISTER'),
                        style: ElevatedButton.styleFrom(
                            fixedSize: const Size(100, 20),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            )),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
